from nerdvision import nv_serverless


@nv_serverless
def handler(event, context):
    name_ = event['name']
    resp = {
        'name': name_
    }
    return resp
